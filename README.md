# Nepali Wallet Python Client

An SDK for payment gateways integration in Nepal.

> **Note**: <i> If you are a contributor or want to contribute to this project,
> you can check the [contribution](CONTRIBUTION.md) section to get started.</i>


Nepali Wallet python client offers a python sdk for the following payment
gateways:

- e-sewa ✅ unstable
- khalti ✅ stable
- ePay (in progress, unstable)
- IMEPay (in progress, unstable)
- IPay (in progress, unstable)
